TARBALL=$1
shift
SPECNAME=$1
shift
MARKER=$1
shift
STARTNUM=$1
shift
LOCALVERSION=$1
shift

SOURCES=rpmbuild/SOURCES
SRPMDIR=rpmbuild/SRPM
SPEC=rpmbuild/SPECS/${SPECNAME}

LOCAL_PYTHON=python3

# Pre-cleaning
rm -rf .tmp asection psection patchlist

cp ${TARBALL} ${SOURCES}/${TARBALL}

# Handle patches
git format-patch --first-parent --no-cover-letter --no-renames -k --no-binary ${MARKER}.. > patchlist
num=${STARTNUM}
for patchfile in `cat patchlist`; do
  ${LOCAL_PYTHON} frh.py ${patchfile} > .tmp
  if grep -q '^diff --git ' .tmp; then
    patchname=$(grep -x "patch_name: .*\.patch" .tmp | sed 's/patch_name: \(.*\)/\1/')
    if [ -n "$patchname" ]; then
	    mv .tmp ${SOURCES}/${patchname}
	    continue
    fi
    let num=num+1
    echo "Patch${num}: ${patchfile}" >> psection
    echo "%patch${num} -p1" >> asection
    mv .tmp ${SOURCES}/${patchfile}
  fi
done

cp ${SPECNAME} ${SPEC}
sed -i '/# Source-git patches/r psection' ${SPEC}
sed -i '/# Apply source-git patches/r asection' ${SPEC}
if [ -n "${LOCALVERSION}" ]; then
  sed -i "s/\(Release: .*%{?dist}\)/\1${LOCALVERSION}/" ${SPEC}
fi

# Post-cleaning
rm -rf $(cat patchlist)
rm -rf .tmp asection psection patchlist
